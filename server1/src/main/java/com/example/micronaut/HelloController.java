package com.example.micronaut;

import io.micronaut.http.annotation.*;

@Controller("/hello")
public class HelloController {

    @Get("/")
    public String index() {
        return "Hello from Server 1";
    }
}