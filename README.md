# micronaut-microservices-discovery


### Architecture Diagram
<img src="architecture.png" alt="Architecture"/>


### Eruka server

-create one spring boot appilication

--add dependency to user spring Eruka server
```
<dependency>
			<groupId>org.springframework.cloud</groupId>
			<artifactId>spring-cloud-starter-netflix-eureka-server</artifactId>
			<version>3.1.0</version>
		</dependency>
``` 

-add configuration in appilication.yml 

```
server:
  port: ${PORT:8761}

eureka:
  client:
    registerWithEureka: false
    fetchRegistry: false
    server:
      waitTimeInMsWhenSyncEmpty: 0
```


-in Main class add `@EnableEurekaServer` annotation
```
@EnableEurekaServer
@SpringBootApplication
public class EurekaServerApplication {

	public static void main(String[] args) {
		SpringApplication.run(EurekaServerApplication.class, args);
	}

}
```

### To register service on Eruka server (client,server1 and server2)

```
 <dependency>
      <groupId>io.micronaut.discovery</groupId>
      <artifactId>micronaut-discovery-client</artifactId>
      <scope>compile</scope>
    </dependency>
```

-add Eruka configuration in appilication.yml

```
eureka:
    client:
        registration:
            enabled: true
        defaultZone: "${EUREKA_HOST:localhost}:${EUREKA_PORT:8761}"
```

### call server api from client

--helloController of client

```
@Controller("/client")
public class HelloClientController {


    private Server1Client server1Client;
    private Server2Client server2Client;

    public HelloClientController(Server1Client server1Client, Server2Client server2Client)
    {
    this.server1Client = server1Client;
    this.server2Client = server2Client;
    }


    @Get("/server1")
    public String server1() {
        return server1Client.helloFromServer1();
    }
    @Get("/server2")
    public String server2() {
        return server2Client.helloFromServer2();
    }
}
```


-- server1 and server2 client
```
@Client(id = "server1")
public interface Server1Client {

    @Get("/hello")
    public String helloFromServer1();
}

@Client(id = "server2")
public interface Server2Client {

    @Get("/hello")
    public String helloFromServer2();
}
```

### Build and Run
- Eureka Server - `cd eureka-server; mvn clean instal` and `java -jar target/eureka-server-0.0.1-SNAPSHOT.jar`
- Client - `cd client; mvn clean instal` and `mvn mn:run`
- Server 1 - `cd server1; mvn clean instal` and `mvn mn:run`
- Server 2 - `cd server2; mvn clean instal` and `mvn mn:run`

### Access URLs
- Eureka Server - <a href="http://localhost:8761/">http://localhost:8761/</a>
- Client - <a href="http://localhost:8081/">http://localhost:8081/</a>
- Server 1 - <a href="http://localhost:8082/">http://localhost:8082/</a>
- Server 2 - <a href="http://localhost:8083/">http://localhost:8083/</a>
 
### Endpoints
- Client - Connect to Server 1 - <a href="http://localhost:8081/server1">http://localhost:8081/client/server1</a>
- Client - Connect to Server 2 - <a href="http://localhost:8081/server2">http://localhost:8081/client/server2</a>
- Server 1 - Hello from Server 1 - <a href="http://localhost:8082/hello">http://localhost:8082/hello</a>
- Server 2 - Hello from Server 2 - <a href="http://localhost:8083/hello">http://localhost:8083/hello</a>
