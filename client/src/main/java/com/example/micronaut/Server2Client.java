package com.example.micronaut;

import io.micronaut.http.client.annotation.Client;
import io.micronaut.http.annotation.Get;

@Client(id = "server2")
public interface Server2Client {

    @Get("/hello")
    public String helloFromServer2();
}